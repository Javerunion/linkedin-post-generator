from login_script import ids_scraping, login_scraping
import time
from random import randint
import pandas as pd
SCROLL_PAUSE_TIME = randint(5,10)
ids_scrap = ids_scraping()
log_scrap = login_scraping()

class get_url(ids_scrap,login_scraping, pandas):
    def __init__(self, log_scrap, ids_scrap):
        self.ids_scrap = ids_scrap
        self.log_scrap = log_scrap
        log_scrap.connect()
        log_scrap.tap_email()
        log_scrap.tap_password()
        log_scrap.log()
    def research_url(self, research_url):
        return self.log_scrap.driver.get(research_url)
    def scrap(self):
        last_height = self.log_scrap.driver.execute_script("return document.body.scrollHeight")
        post = []
        urls = []
        while True:
            time.sleep(SCROLL_PAUSE_TIME)
            self.log_scrap.driver.execute_script("return document.body.scrollHeight")
            time.sleep(SCROLL_PAUSE_TIME)
            elems = self.log_scrap.driver.find_elements_by_xpath("//div[@class='feed-shared-article__link-container']/a[@href]/../../../../div[@class='feed-shared-update-v2__description-wrapper ember-view']")
            quant_urls = self.log_scrap.driver.find_elements_by_xpath("//div[@class='feed-shared-article__link-container']/a[@href]")
            for elem in elems:
                post.append(elem.text)
            for url in quant_urls:
                urls.append(url.get_attribute("href"))
            time.sleep(SCROLL_PAUSE_TIME)
            # Calculate new scroll height and compare with last scroll height
            new_height = self.log_scrap.driver.execute_script("return document.body.scrollHeight")
            if new_height == last_height:
                break
            last_height = new_height

        data_linkedin = {'linkedin_post': post,'article_url' : url}
        return data_linkedin
    def to_csv(self, data_linkedin):
        return pd.DataFrame(data_linkedin).to_csv('data_linkedin.csv')
