from selenium import webdriver
import os
class ids_scraping:
    def __init__(self, path: str, website: str, email: str, password:str, research_url :str):
        self.path = path
        self.website = website
        self.email = email
        self.password = password 
        self.research_url = research_url

class login_scraping(ids_scraping, webdriver):
    def __init__(self, ids_scrap, driver):
        self.driver = driver
        self.ids_scrap = ids_scrap
    def connect(self):
        return self.driver.get(self.ids_scrap.website)
    def tap_email(self):
        username = self.driver.find_element_by_id('session_key')
        return username.send_keys(self.ids_scrap.email)
    def tap_password(self):
        password = self.driver.find_element_by_id('session_password')
        return password.send_keys(self.ids_scrap.password)
    def log(self):
        log_in_button = self.driver.find_element_by_class_name('sign-in-form__submit-button')
        return log_in_button.click()



