import re
from nltk.corpus import stopwords
import nltk
nltk.download('stopwords')
stop_words = stopwords.words('english')
from generate_linkedin_post_from_blog_article.infrastructure.dataclass_linkedin import DataclassLinkedin
#from transformers.models.bart.modeling_bart import shift_tokens_right
from typing import Dict, Any, List
import torch

class BatchLoader:

    def __init__(self, tokenizer, data_kwargs, tpu_num_cores=None):
        self.tokenizer = tokenizer
        self.pad_token_id = tokenizer.pad_token_id
        self.data_kwargs = data_kwargs
        self.tpu_num_cores = tpu_num_cores

    def __call__(self, batch: List[DataclassLinkedin]) -> Dict[str, torch.Tensor]:
        """[summary]

        Args:
            batch (List[DataclassLinkedin]): [list of type DataclassLinkedin]

        Returns:
            Dict[str, torch.Tensor]: [dict with key: 'article' or 'post', and value: the blog article and the linkedin post ]
        """
        batch = self.preprocess(batch)
        batch = self._encode(batch)
        #batch["decoder_input_ids"] = shift_tokens_right(batch["labels"], self.pad_token_id)
        return batch

    def _encode(self, batch: List[DataclassLinkedin]) -> Dict[str, torch.Tensor]:
        """[summary]

        Args:
            batch (List[DataclassLinkedin])

        Returns:
            Dict[str, torch.Tensor]
        """
        batch_encoding = self.tokenizer.prepare_seq2seq_batch(
            src_texts=[dataclass_linkedin.article for dataclass_linkedin in batch],
            tgt_texts=[dataclass_linkedin.post for dataclass_linkedin in batch],
            max_length=512,
            max_target_length=128,
            padding="longest",            # Dynamic padding : each batch has its own max length
            return_tensors="pt",
            **self.data_kwargs,
        )
        return batch_encoding.data

    def preprocess(self, text):
        text = text.lower() # lowercase
        text = text.split() 
        text = " ".join(text)
        text = text.split()
        newtext = []
        for word in text:
            if word not in stop_words:
                newtext.append(word)
        text = " ".join(newtext)
        text = text.replace("'s",'') # convert your's -> your
        text = re.sub(r'\(.*\)','',text) # remove (words)
        text = re.sub(r'[^a-zA-Z0-9. ]','',text) # remove punctuations
        text = re.sub(r'\.',' . ',text)
        return text
