from transformers import Trainer, TrainingArguments
import generate_linkedin_post_from_blog_article.config.config as cf
import torch.nn as nn
import os

class TrainModel():

    def __init__(self, batch, train_dataset,val_dataset, model, tokenizer):
        self.batch = batch
        self.train_dataset=train_dataset
        self.val_dataset = val_dataset
        self.model = model
        self.tokenizer=tokenizer

    def _freeze_params(self,model: nn.Module):
        for par in model.parameters():
            par.requires_grad = False

    def _freeze_embeds(self,model):
        self._freeze_params(model.model.shared)
        for d in [model.model.encoder, model.model.decoder]:
            self._freeze_params(d.embed_positions)
            self._freeze_params(d.embed_tokens)

    def train_model(self):
        self._freeze_embeds(self.model)
        self._freeze_params(self.model.get_encoder())
    
        training_args = TrainingArguments(
        output_dir=cf.MODEL_DIR,          # output directory
        num_train_epochs=10,              # total # of training epochs
        per_device_train_batch_size=1,  # batch size per device during training
        per_device_eval_batch_size=1,   # batch size for evaluation
        warmup_steps=1000,           # number of warmup steps for learning rate scheduler
        learning_rate = 0.001,
        #weight_decay=0.01,  
        #logging_steps=10,             # strength of weight decay
        logging_dir=cf.LOG_DIR,
        overwrite_output_dir=True        # directory for storing logs
        )

        trainer = Trainer(
        model = self.model,
        args = training_args,
        train_dataset = self.train_dataset,
        data_collator = self.batch,
        eval_dataset= self.val_dataset
        )
        trainer.train()
        trainer.save_model(os.path.join(cf.MODELS_DIR,'model'+ cf.MODEL_NAME))



