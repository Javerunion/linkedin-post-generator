from transformers import AutoModelWithLMHead, AutoTokenizer
from transformers import MBartForConditionalGeneration, MBartTokenizer

class Models:

    def __init__(self, model_name):
        self.model_name = model_name

    def _mbart_model(self):
        model_mbart = MBartForConditionalGeneration.from_pretrained("facebook/mbart-large-cc25")
        tokenizer_mbart = MBartTokenizer.from_pretrained("facebook/mbart-large-cc25")
        return model_mbart, tokenizer_mbart

    def _t5_model(self):
        model_t5 = AutoModelWithLMHead.from_pretrained("t5-base")
        tokenizer_t5 = AutoTokenizer.from_pretrained('t5-base')
        return model_t5, tokenizer_t5

    def get_model(self):
        if self.model_name=="t5":
            return self._mbart_model()
        elif self.model_name=="mbart":
            return self._t5_model()
        else:
            print("model_name shoul be t5 or mbart")