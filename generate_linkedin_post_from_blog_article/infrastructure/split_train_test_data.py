from sklearn.model_selection import train_test_split
import generate_linkedin_post_from_blog_article.config.config as cf
from generate_linkedin_post_from_blog_article.infrastructure.load_data import LoadData
import pandas as pd

if __name__ == "__main__":
    data  = LoadData(cf.FULL_DATA_DIR)
    _,_,df = data.list_article_list_post()
    train_val_article, test_article, train_val_post, test_post = train_test_split(df[cf.ARTICLE], df[cf.POST], test_size=.2, )
    
    train_val_data= pd.concat([train_val_article, train_val_post], axis=1)
    train_val_data.to_csv(cf.FULL_DATA_DIR_TRAIN_VAL)

    test_data= pd.concat([test_article, test_post], axis=1)
    test_data.to_csv(cf.FULL_DATA_DIR_TEST)
    
 
