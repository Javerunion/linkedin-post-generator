# -*- coding: UTF-8 -*-
import pandas as pd
import generate_linkedin_post_from_blog_article.config.config as cf
import os 


 
class LoadData:
    """
    cleans customer data from raw file
    """
    def __init__(self, path=""):
        self.path = path


    def list_article_list_post(self):
        
        _ , extension = os.path.splitext(self.path)
        if extension == '.csv':
            df = pd.read_csv(self.path) 
        elif extension == '.parquet':
            df = pd.read_parquet(self.path)
        else:
            raise FileExistsError('Extension must be parquet or csv.')
      
        return df[cf.ARTICLE].values.tolist(), df[cf.POST].values.tolist(), df.copy()


   
